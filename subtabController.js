({
    doInit : function(component, event, helper) {
        var pageReference = component.get("v.pageReference");
          var workspaceAPI = component.find("workspace");
          workspaceAPI
            .getFocusedTabInfo()
            .then(function(response) {
              var focusedTabId = response.subtabs.tabId;
              console.log("focusedTabId " + focusedTabId);
              workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: Label
              });
      
              //Icon for Tab
              workspaceAPI.setTabIcon({
                tabId: focusedTabId,
                icon: "standard:people",
                iconAlt: "Details"
              });
            })
            .catch(function(error) {
              console.log(error);
            });

    },
    handleDetails : function(component, event, helper) {

    var workspaceAPI = component.find("workspace");
    workspaceAPI.getAllTabInfo().then(function(response) {
      console.log("Tab::" + response.parentTabId);
      component.set("v.tabId", tabId);
    });
    workspaceAPI
      .openSubtab({
        pageReference: {
          type: "standard__component",
          attributes: {
            componentName: "c__componentName"
          },
          state: {
            //parameters
          }
        },
        focus: true
      })
      .then(function(response) {
        //Labelling Tab
        workspaceAPI.setTabLabel({
          tabId: response,
          label: "label"
        });

        //Icon for Tab
        workspaceAPI.setTabIcon({
          tabId: response,
          icon: "standard:scan_card",
          iconAlt: "Details"
        });

        workspaceAPI
          .getTabInfo({
            tabId: response
          })
          .then(function(tabInfo) {
            console.log("The recordId for this tab is: " + tabInfo);
          });
      })
      .catch(function(error) {
        console.log(error);
      });
    },
    
})